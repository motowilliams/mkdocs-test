async function handleRequest(request) {
  // Return a new Response based on a URL's hostname
  var url = new URL(request.url)
  const userAgent = (request.headers.get("User-Agent") || "").toLowerCase()

  var init = {
    headers: request.headers,
  }

  if (url.pathname == "/") {
    console.log(`url.pathname: ${url.pathname}`)
    console.log(`Loading user agent script from ${script_config_uri}`)
    const configResponse = await fetch(script_config_uri, {})
    var userAgentScripts = {
      "curl": "https://makedocs.io/init.sh",
      "wget": "https://makedocs.io/init.sh",
      "powershell": "https://makedocs.io/init.ps1",
      "pwsh": "https://makedocs.io/init.ps1"
    }
    for (var key in userAgentScripts) {
      if (userAgent.includes(key)) {
        console.log(`${key}:${userAgentScripts[key]}`);
        url = new URL(userAgentScripts[key])
        continue
      }
    }
  }

  console.log(`Requesting ${url} with headers: ${JSON.stringify(init)}`)
  return await fetch(url, init)
}

addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request))
})
