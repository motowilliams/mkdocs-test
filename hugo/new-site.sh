#!/usr/bin/env bash
COMMIT=$1
DIR=docs
hugo new site $DIR

#https://codeload.github.com/alex-shpak/hugo-book/tar.gz/refs/tags/v9
curl -sL https://codeload.github.com/alex-shpak/hugo-book/zip/refs/tags/v9 --output theme.zip
unzip -o theme.zip -d theme
mkdir -p $DIR/themes/book/
cp -R theme/**/* $DIR/themes/book/
rm -rf $DIR/themes/book/exampleSite
mkdir -p $DIR/content/
cp -R theme/**/exampleSite/content/* $DIR/content/
echo "theme = 'book'" >> "$DIR/config.toml"
echo "#baseURL = '//example.com/optional_subdirectory/'" >> "$DIR/config.toml"
rm -rf theme
rm -rf theme.zip

echo "Coping Makefile"
cp Makefile $DIR
echo "Coping Dockerfile"
cp Dockerfile $DIR
echo "Making CI directory"
mkdir -p "$DIR/ci/"
echo "Coping gitlab pipeline"
cp .gitlab-ci.yml "$DIR/ci/"
echo "Coping github actions"
cp -R .github "$DIR/ci/"

echo "Creating tarball"
tar -czvf "${COMMIT}-hugo-$DIR.tar.gz" $DIR
echo "Creating zip"
zip -r -9 -T "${COMMIT}-hugo-$DIR.zip" $DIR

rm -rf $DIR
