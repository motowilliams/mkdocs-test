GENERATOR=hugo
COMMIT_HASH="${COMMIT_HASH:-0000000000000000000000000000000000000000}"
ARCHIVE="${COMMIT_HASH}-${GENERATOR}-docs.tar.gz"
ARCHIVEURL="https://makedocs.io/$ARCHIVE"
CHECKSUMURL="https://makedocs.io/checksum.txt"
SCRIPT_ROOT=$(pwd)

echo "Downloading"
echo " - archive $CHECKSUMURL sha file to $SCRIPT_ROOT"
curl -sL $CHECKSUMURL | grep -i $ARCHIVE > "$SCRIPT_ROOT/checksum.txt"

echo " - archive $ARCHIVEURL to $SCRIPT_ROOT"
curl -sL $ARCHIVEURL --output "$SCRIPT_ROOT/$ARCHIVE"

echo -n "Validating checksum of "
sha256sum --check "checksum.txt"

echo "Extracting"
echo " - archive $SCRIPT_ROOT/$ARCHIVE"
tar --extract --file "$SCRIPT_ROOT/$ARCHIVE" -C "$SCRIPT_ROOT/"

echo "Cleaning up"
rm -rf "$SCRIPT_ROOT/$ARCHIVE"
rm -rf "$SCRIPT_ROOT/checksum.txt"
