.DEFAULT_GOAL := help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sed -E "s/(.*)?Makefile\://g" | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

export DOCS_DIR_NAME ?= content
export DOCS_SRC ?= docs
export PUBLISH_DIR ?= public
export REPO_URL ?= https://example.com/repo
export SITE_DIR ?= site
export SITE_NAME ?= Sample-Site
export PROTOCOL ?= http
export DOMAIN_NAME ?= 127.0.0.1:8000
export SITE_URL ?= $(PROTOCOL)://$(DOMAIN_NAME)/
export LOCAL_PORT ?= 8000

ifeq ($(OS),Windows_NT)
SHELL := '/usr/bin/sh.exe'
PWD := $(shell sh.exe -c pwd)
else
SHELL := /bin/bash
endif

export BASH_CMD := $(SHELL) -c
export COMMIT_HASH ?= $(shell [[ -d ".git" ]] && git rev-parse HEAD)
export SHORT_COMMIT_HASH ?= $(shell [[ -d ".git" ]] && git rev-parse --short HEAD)
export REPO_ROOT := $(shell [[ -n "${CI}" ]] && echo $(PWD) || echo '/app' )
export VERSION ?= $(shell [[ -d ".git" ]] && git tag --sort=v:refname | grep -E '[0-9]' | tail -1 || echo 0.0.0)

export IMAGE_NAME ?= makedocsio
export IMAGE_VERSION ?= latest
export IMAGE_TAG = $(IMAGE_NAME):$(IMAGE_VERSION)
export ADDITIONAL_BUILD_ARGS ?=

export DOCS_SRC_PATH=$(REPO_ROOT)/$(DOCS_SRC)
export PUBLISH_PATH := $(REPO_ROOT)/$(PUBLISH_DIR)

export CONTENT_SITE_PATH=$(DOCS_SRC_PATH)/$(DOCS_DIR_NAME)
export DOCS_SITE_PATH=$(DOCS_SRC_PATH)/$(SITE_DIR)

ifdef CI
	export IMAGE_TAG=
	export DOCKER_COMMAND :=
else
	export DOCKER_COMMAND := docker run -it \
		-v $(PWD):$(REPO_ROOT) \
		--env CI=TRUE \
		--env COMMIT_HASH=$(COMMIT_HASH) \
		--env COMMIT_HASH_LENGTH=$(COMMIT_HASH_LENGTH) \
		--env CONTENT_SITE_PATH=$(CONTENT_SITE_PATH) \
		--env DOCS_DIR=$(DOCS_DIR_NAME) \
		--env DOCS_SITE_PATH=$(DOCS_SITE_PATH) \
		--env DOCS_SRC_PATH=$(DOCS_SRC_PATH) \
		--env LOCAL_PORT=$(LOCAL_PORT) \
		--env PUBLISH_PATH=$(PUBLISH_PATH) \
		--env REPO_ROOT=$(REPO_ROOT) \
		--env REPO_URL=$(REPO_URL) \
		--env SITE_DIR=$(SITE_DIR) \
		--env SITE_NAME="$(SITE_NAME)" \
		--env SITE_URL=$(SITE_URL) \
		--name $(IMAGE_NAME) \
		--rm
endif

bump_patch:
	$(eval CMD := semver bump patch $(VERSION) )
	@BUMP=$(shell $(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)') && \
	echo "Bumping version from $(VERSION) to $$BUMP" && \
	git tag -a -m "$$BUMP" $$BUMP

docker_build:
	docker build --pull $(ADDITIONAL_BUILD_ARGS) -t $(IMAGE_TAG) . -f Dockerfile

debug: ## Runs the docker image interactively for debugging purposes
	@$(DOCKER_COMMAND) $(ADDITIONAL_BUILD_ARGS) $(IMAGE_TAG) $(CMD)

clean_makedocsio:
	$(eval CMD := \
	mkdir -p $(REPO_ROOT)/public && rm -rf $(REPO_ROOT)/public/* && \
	rm -rf $(REPO_ROOT)/hugo/*-hugo-docs.tar.gz && rm -rf $(REPO_ROOT)/hugo/*-hugo-docs.zip && \
	rm -rf $(REPO_ROOT)/mkdocs/*-mkdocs-docs.tar.gz && rm -rf $(REPO_ROOT)/mkdocs/*-mkdocs-docs.zip && \
	rm -rf $(DOCS_SITE_PATH) \
	)
	@echo "Cleaning $(SITE_DIR).zip & $(SITE_DIR) in $(SITE_DIR)"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

build_makedocsio:
	$(eval CMD := echo "Starting build" && \
	build && \
	cp /theme.list $(DOCS_SITE_PATH)/ && \
	echo "Post build processing complete" \
	)
	@echo "Building"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_pre_process:
	$(eval CMD := echo "Pre-processing" && \
	sed -i -E "s/\$?SOURCE_URL\s?\=\s?.*/SOURCE_URL\=$(PROTOCOL)\:\/\/$(DOMAIN_NAME)/g" $(CONTENT_SITE_PATH)/*.sh && \
	sed -i -E "s/\$?SOURCE_URL\s?\=\s?.*/\$`SOURCE_URL\ = \"$(PROTOCOL)\:\/\/$(DOMAIN_NAME)\"/g" $(CONTENT_SITE_PATH)/*.ps1 && \
	sed -i -E "s/\$?VERSION\s?\=\s?.*/VERSION\=$(VERSION)/g" $(CONTENT_SITE_PATH)/*.sh && \
	sed -i -E "s/\$?VERSION\s?\=\s?.*/\$`VERSION\=\"$(VERSION)\"/g" $(CONTENT_SITE_PATH)/*.ps1 && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/COMMIT_HASH\=$(COMMIT_HASH)/g" $(CONTENT_SITE_PATH)/*.sh && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/\$`COMMIT_HASH\=\"$(COMMIT_HASH)\"/g" $(CONTENT_SITE_PATH)/*.ps1 && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/COMMIT_HASH\=$(COMMIT_HASH)/g" $(REPO_ROOT)/hugo/*.sh && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/\$`COMMIT_HASH\=\"$(COMMIT_HASH)\"/g" $(REPO_ROOT)/hugo/*.ps1 && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/COMMIT_HASH\=$(COMMIT_HASH)/g" $(REPO_ROOT)/mkdocs/*.sh && \
	sed -i -E "s/\$?COMMIT_HASH\s?\=\s?.*/\$`COMMIT_HASH\=\"$(COMMIT_HASH)\"/g" $(REPO_ROOT)/mkdocs/*.ps1 \
	)
	@echo "Pre-processing"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_all: publish_pre_process publish_mkdocs publish_hugo publish_makedocsio
	$(eval CMD := cd $(REPO_ROOT)/public && sha256sum *-docs.* > checksum.txt)
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_makedocsio: build_makedocsio
	$(eval CMD := \
	mkdir -p $(PUBLISH_PATH) && \
	cp -R $(DOCS_SITE_PATH)/* $(PUBLISH_PATH)/ && \
	echo "Publishing" \
	)
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_mkdocs:
	$(eval CMD := \
	cd $(REPO_ROOT)/mkdocs && \
	new-mkdocs-site $(COMMIT_HASH) && \
	mkdir -p $(REPO_ROOT)/public && \
	cp -f $(REPO_ROOT)/mkdocs/*mkdocs-docs.* $(REPO_ROOT)/public/ && \
	mv $(REPO_ROOT)/public/mkdocs-docs.ps1   $(REPO_ROOT)/public/$(COMMIT_HASH)-mkdocs-docs.ps1 && \
	mv $(REPO_ROOT)/public/mkdocs-docs.sh    $(REPO_ROOT)/public/$(COMMIT_HASH)-mkdocs-docs.sh \
	)
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

publish_hugo:
	$(eval CMD := \
	cd $(REPO_ROOT)/hugo && \
	new-hugo-site $(COMMIT_HASH) && \
	mkdir -p $(REPO_ROOT)/public && \
	cp -f $(REPO_ROOT)/hugo/*hugo-docs.* $(REPO_ROOT)/public/ && \
	mv $(REPO_ROOT)/public/hugo-docs.ps1   $(REPO_ROOT)/public/$(COMMIT_HASH)-hugo-docs.ps1 && \
	mv $(REPO_ROOT)/public/hugo-docs.sh    $(REPO_ROOT)/public/$(COMMIT_HASH)-hugo-docs.sh \
	)
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

package_makedocs:
	$(eval CMD := cd $(DOCS_SRC_PATH) && zip -r ../$(SITE_DIR).zip ./)
	@echo "Packaging"
	$(DOCKER_COMMAND) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

serve_makedocsio:
	$(eval CMD := serve)
	$(DOCKER_COMMAND) -p $(LOCAL_PORT):$(LOCAL_PORT) --expose $(LOCAL_PORT) $(IMAGE_TAG) $(BASH_CMD) '$(CMD)'

create_changelog:
	echo "No change log entries to show at this time" > CHANGELOG && \
    docker run --rm --env _GIT_LIMIT=366 --env _GIT_LOG_OPTIONS="--ignore-all-space --ignore-blank-lines" -v $(PWD):/git arzzen/git-quick-stats /bin/bash -c "git-quick-stats -c | tail -n +4 | uniq | tee CHANGELOG"

print-%: ; @echo $*=$($*)

printenv:
	printenv | sort
