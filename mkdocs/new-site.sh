#!/usr/bin/env bash
COMMIT=$1
DIR=docs
mkdocs new $DIR
echo "site_name: \"My Docs Site\""                  > $DIR/mkdocs.yml
echo "theme: material"                             >> $DIR/mkdocs.yml
echo "plugins:"                                    >> $DIR/mkdocs.yml
echo "  - search"                                  >> $DIR/mkdocs.yml
echo "  - macros:"                                 >> $DIR/mkdocs.yml









echo "Coping Makefile"
cp Makefile $DIR
echo "Coping Dockerfile"
cp Dockerfile $DIR
echo "Making CI directory"
mkdir -p "$DIR/ci/"
echo "Coping gitlab pipeline"
cp .gitlab-ci.yml "$DIR/ci/"
echo "Coping github actions"
cp -R .github "$DIR/ci/"

echo "Creating tarball"
tar -czvf "${COMMIT}-mkdocs-$DIR.tar.gz" $DIR
echo "Creating zip"
zip -r -9 -T "${COMMIT}-mkdocs-$DIR.zip" $DIR

rm -rf $DIR
