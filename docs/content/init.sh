#!/usr/bin/env bash
SOURCE_URL="${SOURCE_URL:-http://127.0.0.1:8000}"
COMMIT_HASH="${COMMIT_HASH:-0000000000000000000000000000000000000000}"
VERSION="${VERSION:-9.9.9}"
PATH_SEPERATOR='\'

CWD=${PWD##*/}
DOCS_DIR_DEFAULT=docs

echo
echo "**********************************************************************************"
echo "*                                                                                *"
echo "*  Welcome to makedocs.io - Follow the prompts & let's create your project docs  *"
echo "*                                                                                *"
echo "**********************************************************************************"
echo "*  version: $VERSION              commit: $COMMIT_HASH  *"
echo "**********************************************************************************"
echo

echo "Would you like to install Mkdocs or Hugo?"
select yn in "Mkdocs" "Hugo" "None"; do
    case $yn in
        Mkdocs ) GENERATOR=mkdocs; break;;
        Hugo )   GENERATOR=hugo;     break;;
        None )   echo "Thanks for visiting makedocs.io!" ;exit; break;;
    esac
done


[[ -f ".gitlab-ci.yml" ]] && GITLAB_EXISTS=true || GITLAB_EXISTS=false
[[ -d ".github" ]] && GITHUB_EXISTS=true || GITHUB_EXISTS=false


if [[ "$GITLAB_EXISTS" == "false" && "$GITHUB_EXISTS" == "false" ]]; then
    echo "There is no GitHub or Gitlab CI found. Would you like to start one?"
    select yn in "GitHub" "Gitlab" "None"; do
        case $yn in
            GitHub ) SKIP_CI=false; ADD_GITHUB_PIPELINE=true; break;;
            Gitlab ) SKIP_CI=false; ADD_GITLAB_PIPELINE=true; break;;
            None ) SKIP_CI=true; break;;
        esac
    done
fi

if [[ "$SKIP_CI"="false" && "$GITLAB_EXISTS" == "false" && "$GITHUB_EXISTS" == "true" ]]; then
    echo "Would you like to add to existing GitHub Actions?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) ADD_GITHUB_PIPELINE=true; break;;
            No ) ADD_GITHUB_PIPELINE=false; break;;
        esac
    done
fi

if [[ "$SKIP_CI"="false" && "$GITLAB_EXISTS" == "true" && "$GITHUB_EXISTS" == "false" ]]; then
    echo "Would you like to add to existing Gitlab CI stages?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) ADD_GITLAB_PIPELINE=true; break;;
            No ) ADD_GITLAB_PIPELINE=false; break;;
        esac
    done
fi

echo "Downloading installation script for $GENERATOR"
bash <(curl -sL http://makedocs.io/${COMMIT_HASH}-${GENERATOR}-docs.sh#${COMMIT_HASH})

if [[ "$SKIP_CI"="false" ]]; then
    SUBMAKEFILE="docs/Makefile"
    if [[ "$ADD_GITHUB_PIPELINE" == "true" ]]; then
        echo "Adding GitHub Actions to project"
        mkdir -p .github
        mkdir -p .github/workflows
        cat $DOCS_DIR_DEFAULT/ci/.github/workflows/makedocs-generation.yml >> .github/workflows/makedocs-generation.yml
        # GitHub Pages are published into a site directory
        sed -i -E "s/export PUBLISH_DIR:=.*/export PUBLISH_DIR:=site/g" $SUBMAKEFILE
    fi

    if [[ "$ADD_GITLAB_PIPELINE" == "true" ]]; then
        echo "Adding Gitlab Pipelines to project"
        cat $DOCS_DIR_DEFAULT/ci/.gitlab-ci.yml >> .gitlab-ci.yml
        # Gitlab Pages are published into a public directory
        sed -i -E "s/export PUBLISH_DIR:=.*/export PUBLISH_DIR:=public/g" $SUBMAKEFILE
    fi
fi

echo "Adding Makefile include to $GENERATOR sub Makefile"
echo "include $DOCS_DIR_DEFAULT/Makefile" >> Makefile

echo "Cleaning up"
rm -rf $DOCS_DIR_DEFAULT/ci
