[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$SOURCE_URL = if ($env:SOURCE_URL) { $env:SOURCE_URL } else { "http://127.0.0.1:8000" };
$COMMIT_HASH = if ($env:COMMIT_HASH) { $env:COMMIT_HASH } else { "0" * 40 };
$VERSION = if ($env:VERSION) { $env:VERSION } else { "9.9.9" };
$PATH_SEPERATOR = [IO.Path]::DirectorySeparatorChar

$CWD = Get-Item -Path .\ | Select-Object -ExpandProperty Name
$DOCS_DIR_DEFAULT = "docs"

Write-Host
Write-Host "**********************************************************************************"
Write-Host "*                                                                                *"
Write-Host "*  Welcome to makedocs.io - Follow the prompts & let's create your project docs  *"
Write-Host "*                                                                                *"
Write-Host "**********************************************************************************"
Write-Host "*  version: $VERSION              commit: $COMMIT_HASH  *"
Write-Host "**********************************************************************************"
Write-Host

$SKIP_CI = $true
$ADD_GITHUB_PIPELINE = $false
$ADD_GITLAB_PIPELINE = $false

function Get-ChoiceValue {
    [CmdletBinding()]
    param(
        [string]$Title,
        [string]$Message,
        [array]$Options
    )

    $optionArray = [System.Management.Automation.Host.ChoiceDescription[]] @()
    $Options | ForEach-Object {
        if (($($_) -like "*&*")) {
            $optionArray += New-Object System.Management.Automation.Host.ChoiceDescription $_, (($_) -replace "&", "")
        }
        else {
            $optionArray += "&$($_)"
        }
    }
    $result = $host.ui.PromptForChoice($Title, $Message, $optionArray, 0)
    return ($Options[$result]) -replace "&", ""
}
function Get-BooleanValue {
    [CmdletBinding()]
    param(
        [string]$Title,
        [string]$Message,
        [boolean]$Default
    )

    $index = if ($Default) { 0 } else { 1 }
    $enabled = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Enable $Title"
    $disabled = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Disable $Title"
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($enabled, $disabled)
    $result = $Host.UI.PromptForChoice($Title, $Message, $options, $index)
    $flag = if ($result) { $false } else { $true }
    return $flag
}

$GENERATOR = Get-ChoiceValue -Message "Would you like to install Mkdocs or Hugo?" -Options @( "&mkdocs", "&hugo" )

if (Test-Path -Path ".github") { $GITHUB_EXISTS = $true } else { $GITHUB_EXISTS = $false }
if (Test-Path -Path ".gitlab-ci.yml") { $GITLAB_EXISTS = $true } else { $GITLAB_EXISTS = $false }

if ($GITLAB_EXISTS -eq $false -and $GITHUB_EXISTS -eq $false) {
    $RESULT = Get-ChoiceValue `
        -Message "There is no CI pipeline found. Would you like to start one?" `
        -Options @( "&none", "git&hub", "git&lab" )

    switch ($RESULT) {
        "github" { $SKIP_CI = $false; $ADD_GITHUB_PIPELINE = $true }
        "gitlab" { $SKIP_CI = $false; $ADD_GITLAB_PIPELINE = $true }
        "&none" { $SKIP_CI = $true; $ADD_GITLAB_PIPELINE = $false; $ADD_GITHUB_PIPELINE = $false }
        Default { $SKIP_CI = $true; }
    }
}
elseif ($GITHUB_EXISTS -eq $true) {
    $ADD_GITHUB_PIPELINE = Get-BooleanValue  -Message "Would you like to add to existing GitHub Actions?" -Default $false
}
elseif ($GITLAB_EXISTS -eq $true) {
    $ADD_GITLAB_PIPELINE = Get-BooleanValue -Message "Would you like to add to existing Gitlab pipeline?" -Default $false
}

Write-Host "Downloading installation script for $GENERATOR"
Invoke-RestMethod "http://makedocs.io/$COMMIT_HASH-$GENERATOR-docs.ps1#$COMMIT_HASH" | Invoke-Expression

if ($SKIP_CI -eq $false) {
    $subMakeFile = "$DOCS_DIR_DEFAULT/Makefile"
    if ($ADD_GITHUB_PIPELINE -eq $true) {
        Write-Host "Adding GitHub Actions to project"
        New-Item -Force -ItemType Directory -Path ".github" | Out-Null
        New-Item -Force -ItemType Directory -Path ".github/workflows" | Out-Null
        Get-Content $DOCS_DIR_DEFAULT/ci/.github/workflows/makedocs-generation.yml >> .github/workflows/makedocs-generation.yml
        # GitHub Pages are published into a site directory
        sed -i -E "s/export PUBLISH_DIR:=.*/export PUBLISH_DIR:=site/g" $subMakeFile
        (Get-Content $subMakeFile) -replace 'export PUBLISH_DIR:=.*', 'export PUBLISH_DIR:=site' | Out-File $subMakeFile
    }

    if ( $ADD_GITLAB_PIPELINE -eq $true) {
        Write-Host "Adding Gitlab Pipelines to project"
        Add-Content -Path ".gitlab-ci.yml" -Value (Get-Content "$DOCS_DIR_DEFAULT/ci/.gitlab-ci.yml")
        # Gitlab Pages are published into a public directory
        (Get-Content $subMakeFile) -replace 'export PUBLISH_DIR:=.*', 'export PUBLISH_DIR:=public' | Out-File $subMakeFile
    }
}

Write-Host "Adding Makefile include to $GENERATOR sub Makefile"
Add-Content -Value "include $DOCS_DIR_DEFAULT/Makefile" -Path "Makefile"

Write-Host "Cleaning up"
Remove-Item -Recurse -Force (Join-Path -Path $DOCS_DIR_DEFAULT -ChildPath "ci")
