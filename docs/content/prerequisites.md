# Prerequisites

## Docker Desktop

For creating new content (unless you like iterating with your CI) you'll want to run this locally with Docker Desktop https://www.docker.com/products/docker-desktop

## Make

Yes *that* Make, the one created in 1976! 🤦 We'll be adding some lightweight inspectors shortly for other build systems. For example if you're using Psake or Invoke-Build we'll add tasks for that.
