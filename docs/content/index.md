# {{ config.site_name }}

Creating docs for software projects is not glamorous. MakeDocs will help you get started with some simple docs quickly and easily. It is just some tooling, scripts really, to ask you a couple of questions combined with some cursory inspection of your repo to get you started with a static site generator under some build infrastructure.

This will install a copy of the toolchain needed to create docs. Afterwards you can adjust anything required to make it work better in your project.

!!! tip "Let's Go!"
    Linux/WSL/etc
    ![Placeholder](img/bash.png){align=left}
    ``` bash
    bash <(curl -sL makedocs.io)
    ```

    Windows
    ![Placeholder](img/powershell.png){align=left}
    ``` powershell
    irm makedocs.io | iex
    ```

## How Does It Work?

You execute the installation script for MakeDocs from one of the above commands. It will prompt you to choose a generator, check your repository for existing GitHub Actions or Gitlab Pipelines or ask to install new CICD templates.

Afterwards we grab a package related to your selections and install the generator with optional automation files.

### Flows

``` mermaid
sequenceDiagram
    participant makedocs.io
    participant Terminal
    participant Script
    participant Repo
    Terminal->>makedocs.io: Hello makedocs.io
    Terminal->>makedocs.io: I am not a browser
    makedocs.io->>Terminal: Html? No, take a script!
    Terminal->>Script: Do your thing!
    Script->>Terminal: What static site generator would you like?
    Terminal->>Script: "Mkdocs or Hugo"
    Script->>Terminal: Would you like to install CICD pipelines?
    Terminal->>Script: "Always!"
    Script->>Repo: Let's do this!
    activate Repo
    Script-->>Repo: Downloads package
    Script-->>Repo: Installs CICD pipelines files
    deactivate Repo
    Script->>Terminal: All done!
    Terminal-->Terminal: make docs_docker_build
    Terminal-->Terminal: make docs_build
    Terminal-->Terminal: make docs_serve
```

### Project layout

Using all the defaults for the installation this is what the new additions to your source tree will look like!

    ├─ Makefile                   # Appended include statement for the sub-Makefile
    └─ docs                       # Configurable documentation system directory
        ├─ Dockerfile             # Dockerfile used to build/serve your site
        ├─ Makefile               # Sub-Makefile to provide automation
        ├─ docs|content           # Documentation site content directory
        │   └─ index.md           # The documentation homepage with sample content
        └─ mkdocs.yml|config.toml # Static site's configuration file

### The Scripts

You can either download the scripts below or just run them from our url at {{ config.site_url }}. <a href="https://en.wikipedia.org/wiki/Content_negotiation" target="_blank">Thanks Content Negotiation!</a>

#### via Bash

=== "Execute"

    ``` bash
    bash <(curl -sL makedocs.io)
    ```

=== "Bootstrapper"

    ``` bash
    {{ include_file('content/init.sh','    ') }}
    ```

=== "Mkdocs"

    ``` bash
    {{ include_file('../mkdocs/mkdocs-docs.sh','    ') }}
    ```

=== "Hugo"

    ``` bash
    {{ include_file('../hugo/hugo-docs.sh','    ') }}
    ```

#### via PowerShell

=== "Execute"

    ``` powershell
    irm makedocs.io | iex
    ```

=== "Bootstrapper"

    ``` powershell
    {{ include_file('content/init.ps1','    ') }}
    ```

=== "Mkdocs"

    ``` bash
    {{ include_file('../mkdocs/mkdocs-docs.ps1','    ') }}
    ```

=== "Hugo"

    ``` bash
    {{ include_file('../hugo/hugo-docs.ps1','    ') }}
    ```

## Dockerfile

The docker image is used for local development as well as in CICD for deploying your documentation along with your project.

=== "Execute"

    ``` bash
    make docs_docker_build
    ```

=== "Mkdocs"

    ``` Dockerfile
    {{ include_file('../mkdocs/Dockerfile','    ') }}
    ```

=== "Hugo"

    ``` Dockerfile
    {{ include_file('../hugo/Dockerfile','    ') }}
    ```

## Build it with Make

Part of the installation is the addition of a sub-make file. This wraps all the tasks needed to build, serve and publish your docs. We will add a small include in your root Makefile if we detect one.

=== "Execute"

    | Make Command      | Description                                                        |
    | ----------------- | ------------------------------------------------------------------ |
    | docs_build        | Builds the content to the 'site' directory                         |
    | docs_clean        | Removes the content artifacts directories and archive              |
    | docs_debug        | Runs the docker image interactively for debugging purposes         |
    | docs_docker_build | Build the docker image used for these make targets not within DIND |
    | docs_package      | Builds the content to a compressed archive                         |
    | docs_publish      | Builds the content to the 'PUBLISH_DIR' directory                  |
    | docs_serve        | Runs the static site generation server                             |

=== "Makefile"

    ``` Makefile
    {{ include_file('../Makefile','    ') }}
    ```

<sub><sup>
<a target="_blank" href="https://gitlab.com/motowilliams/makedocsio/-/commit/{{git.short_commit}}">commit: {{ git.short_commit}} ({{ git.date}}) by {{ git.author}} {{ git.message }}</a>
</sup></sub>
